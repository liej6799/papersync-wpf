﻿
using Caliburn.Micro;
using PaperSyncWPF.Misc.EventModels;
using PaperSyncWPF.Models.User;
using PaperSyncWPF.ViewModels.Main;
using PaperSyncWPF.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.ViewModels
{
    public class ShellViewModel : Conductor<object>, IHandle<SignUpVerificationPageEvent>, IHandle<HomeScreenPageEvent>, IHandle<LoginScreenPageEvent>, IHandle<AddPaperScreenPageEvent>
    {
        private IEventAggregator _events;
        private SimpleContainer _container;

        private SignUpVerificationViewModel _signUpVerificationViewModel;
        private HomeScreenViewModel _homeScreenViewModel;
        private AddPaperScreenViewModel _addPaperScreenViewModel;
        private ILogonUserModel _logonUserModel;

        private string _loggedInUsername;

        public string LoggedInUsername
        {
            get { return _loggedInUsername; }
            set {
                _loggedInUsername = value; }
        }


        public ShellViewModel(IEventAggregator events,
            SimpleContainer container, SignUpVerificationViewModel signUpVerificationViewModel, 
            HomeScreenViewModel homeScreenViewModel, ILogonUserModel logonUserModel, AddPaperScreenViewModel addPaperScreenViewModel)
        {
            _events = events;
            _container = container;
            _signUpVerificationViewModel = signUpVerificationViewModel;
            _homeScreenViewModel = homeScreenViewModel;
            _logonUserModel = logonUserModel;
            _addPaperScreenViewModel = addPaperScreenViewModel;

            _events.Subscribe(this);

            // Create new instanfes of login, clear everything when passed.
            UpdateLoggedInUsername();
            ActivateItem(_container.GetInstance<LoginViewModel>());
        }

        public void Handle(SignUpPageEvent message)
        {
            ClearLoginDetails();
            UpdateLoggedInUsername();
            ActivateItem(_container.GetInstance<SignUpViewModel>());
        }

        public void Handle(SignUpVerificationPageEvent message)
        {
            ActivateItem(_signUpVerificationViewModel);
        }

        public void Handle(HomeScreenPageEvent message)
        {
            UpdateLoggedInUsername();
            ActivateItem(_homeScreenViewModel);
        }

        public void Handle(LoginScreenPageEvent message)
        {
            ActivateItem(_container.GetInstance<LoginViewModel>());
        }

        public void LoginMenu()
        {
            ClearLoginDetails();
            UpdateLoggedInUsername();
            ActivateItem(_container.GetInstance<LoginViewModel>());
        }

        public void SignUpMenu()
        {
            ClearLoginDetails();
            UpdateLoggedInUsername();
            ActivateItem(_container.GetInstance<SignUpViewModel>());
        }

        public void SignUpVerificationMenu()
        {
            ActivateItem(_container.GetInstance<SignUpVerificationViewModel>());
        }

        public void UpdateLoggedInUsername()
        {
            _loggedInUsername = "You have not logged in";
            if (_logonUserModel.username != null)
            {
                _loggedInUsername = _logonUserModel.username;
            }
            NotifyOfPropertyChange(() => LoggedInUsername);
        }

        // Clear Login Details
        public void ClearLoginDetails()
        {
            _logonUserModel.username = null;
            _logonUserModel.token = null;

        }

        public void Handle(AddPaperScreenPageEvent message)
        {
            ActivateItem(_addPaperScreenViewModel);

        }
    }
}

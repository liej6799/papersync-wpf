﻿using Caliburn.Micro;
using FluentValidation.Results;
using PaperSyncWPF.Logic.Network;
using PaperSyncWPF.Logic.Validation;
using PaperSyncWPF.Misc.EventModels;
using PaperSyncWPF.Misc.Helpers;
using PaperSyncWPF.Models.Paper;
using PaperSyncWPF.Models.User;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.ViewModels.Main
{
    public class AddPaperScreenViewModel : Screen
    {
        private IEventAggregator _events;
        private IMessageBoxService _messageBoxService;
        private IAPIHandler _apiHandler;
        private ILogonUserModel _logonUserModel;

        public AddPaperScreenViewModel(IEventAggregator events, IMessageBoxService messageBoxService,
            IAPIHandler apiHandler, ILogonUserModel logonUserModel)
        {
            _events = events;
            _messageBoxService = messageBoxService;
            _apiHandler = apiHandler;
            _logonUserModel = logonUserModel;
        }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _author;

        public string Author
        {
            get { return _author; }
            set { _author = value; }
        }

        private string _doi;

        public string DOI
        {
            get { return _doi; }
            set { _doi = value; }
        }

        private int _year;

        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        public void BackToMainMenu()
        {
            _events.PublishOnUIThread(new HomeScreenPageEvent());
        }

        public void BrowseFile()
        {
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Filter = "PDF Files|*.pdf";
            // Launch OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = openFileDlg.ShowDialog();
            
            // Get the selected file name and display in a TextBox.
            // Load content of file in a TextBlock
            if (result == true)
            {
                _fileName = openFileDlg.FileName;
                NotifyOfPropertyChange(() => FileName);
            }
        }

        public async void SubmitPaper()
        {
            PaperModel paperModel = new PaperModel()
            {
                author = _author,
                description = _description,
                doi = _doi,
                file = _fileName,
                title = _title,
                year = _year
            };

            // Create a Validation
            var res = await Task.Run(() => new AddPaperValidation().Validate(paperModel));

            if (!res.IsValid)
            {
                PrintValidationError(res);
            }
            else
            {
                // Pass the data to the API
                paperModel.fileInBase64 = Convert.ToBase64String(File.ReadAllBytes(paperModel.file));
                _apiHandler.AddNewPaper(paperModel, _logonUserModel.token);
            }
        }

        public void PrintValidationError(ValidationResult data)
        {
            //lbl_signup_validation_errors.Visibility = Visibility.Visible;
            string validation_error = "Error Messages: " + Environment.NewLine;
            // Show Error
            foreach (ValidationFailure failure in data.Errors)
            {
                validation_error += failure.ErrorMessage + Environment.NewLine;
            }
            //lbl_signup_validation_errors.Content = validation_error;
            _messageBoxService.MessageBoxTypes(validation_error, "Validation Error", MessageTypes.Error);
        }
    }
}

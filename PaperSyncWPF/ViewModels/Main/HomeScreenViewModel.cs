﻿using Caliburn.Micro;
using PaperSyncWPF.Misc.EventModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PaperSyncWPF.ViewModels.Main
{
    public class HomeScreenViewModel : Screen
    {
        private IEventAggregator _events;
        public HomeScreenViewModel(IEventAggregator events)
        {
            _events = events;
        }

        public void AddPaper()
        {
            _events.PublishOnUIThread(new AddPaperScreenPageEvent());
        }
    }
}

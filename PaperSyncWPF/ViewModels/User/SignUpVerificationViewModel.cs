﻿using Caliburn.Micro;
using FluentValidation.Results;
using PaperSyncWPF.Logic.Network;
using PaperSyncWPF.Logic.Validation;
using PaperSyncWPF.Misc.EventModels;
using PaperSyncWPF.Misc.Helpers;
using PaperSyncWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PaperSyncWPF.Models.APIModels;

namespace PaperSyncWPF.ViewModels.User
{
    public class SignUpVerificationViewModel : Screen, IHandle<SignUpVerificationPageEvent>
    {
        private IAPIHandler _apiHandler;
        private IEventAggregator _events;
        private string _username;
        private IMessageBoxService _messageBoxService;

        public SignUpVerificationViewModel(IAPIHandler apiHandler, IEventAggregator events, IMessageBoxService messageBoxService)
        {
            _apiHandler = apiHandler;
            _events = events;
            _messageBoxService = messageBoxService;
            _events.Subscribe(this);
        }
        private string _code;

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public async void Submit()
        {
            // Check with the Server
            var a = _code;
            ConfirmUser confirmUser = new ConfirmUser()
            {
                Code = _code,
                Username = _username
            };

            // Code cannot null
            var res = await Task.Run(() => new SignUpVerificationValidation().Validate(confirmUser));

            if (!res.IsValid)
            {
                PrintValidationError(res);
            }
            else
            {
                // Validate data with the API
                var data = _apiHandler.ConfirmRegister(confirmUser);
                if (data.statusCodeSpecs == StatusCodeSpecs.INVALID_VERIFICATION)
                {
                    _messageBoxService.MessageBoxTypes(data.message, StatusCodeSpecs.INVALID_VERIFICATION.ToString(), MessageTypes.Error);
                }
                else if (data.statusCodeSpecs == StatusCodeSpecs.LOGIN_REQUIRED || data.statusCodeSpecs == StatusCodeSpecs.USER_ENTER_SUCCESS)
                {
                    _events.PublishOnUIThread(new LoginScreenPageEvent());
                }
            }
            
        }

        public void PrintValidationError(ValidationResult data)
        {
            //lbl_signup_validation_errors.Visibility = Visibility.Visible;
            string validation_error = "Error Messages: " + Environment.NewLine;
            // Show Error
            foreach (ValidationFailure failure in data.Errors)
            {
                validation_error += failure.ErrorMessage + Environment.NewLine;
            }
            //lbl_signup_validation_errors.Content = validation_error;
            _messageBoxService.MessageBoxTypes(validation_error, "Validation Error", MessageTypes.Error);
        }

        public void Handle(SignUpVerificationPageEvent message)
        {
            _username = message._username;
        }
    }
}

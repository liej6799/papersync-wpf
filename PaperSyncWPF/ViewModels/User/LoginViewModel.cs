﻿using Caliburn.Micro;
using FluentValidation.Results;
using PaperSyncWPF.Logic.Network;
using PaperSyncWPF.Logic.Validation;
using PaperSyncWPF.Misc.EventModels;
using PaperSyncWPF.Misc.Helpers;
using PaperSyncWPF.Models;
using PaperSyncWPF.Models.User;
using System;
using System.Threading.Tasks;

namespace PaperSyncWPF.ViewModels.User
{
    public class LoginViewModel : Screen
    {
        private string _userName;
        private string _passWord;
        private IMessageBoxService _messageBoxService;
        private IAPIHandler _apiHandler;
        private IEventAggregator _events;
        private ILogonUserModel _logonUserModel;

        public LoginViewModel(IEventAggregator events, IMessageBoxService messageBoxService, IAPIHandler apiHandler,
            ILogonUserModel logonUserModel)
        {
            _events = events;
            _messageBoxService = messageBoxService;
            _apiHandler = apiHandler;
            _logonUserModel = logonUserModel;
        }
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                NotifyOfPropertyChange(() => UserName);
                _userName = value;
            }
        }

        public string PassWord
        {
            get
            {
                return _passWord;
            }
            set
            {
                NotifyOfPropertyChange(() => PassWord);
                _passWord = value;
            }
        }

        public async void Login()
        {
            // Check username and password

            // Pass the data to the API
            UserModel userModel = new UserModel()
            {
                username = _userName,
                password = _passWord
            };

            var res = await Task.Run(() => new LoginValidation().Validate(userModel));

            if (!res.IsValid)
            {
                PrintValidationError(res);
            }
            else
            {
                // Check Via API
                var login_res = await Task.Run(() => _apiHandler.LoginUser(userModel));
                if (login_res == null)
                {
                    _messageBoxService.MessageBoxTypes("There are problems connecting to the server. Please try again later.", "Connection Error", MessageTypes.Error);
                    return;
                }
                if (login_res.statusCodeSpecs == StatusCodeSpecs.USER_NOT_CONFIRM)
                {
                    _messageBoxService.MessageBoxTypes(login_res.message, StatusCodeSpecs.USER_NOT_CONFIRM.ToString(), MessageTypes.Normal);
                    // redirect user to new page
                    

                }
                else if (login_res.statusCodeSpecs == StatusCodeSpecs.INVALID_LOGIN)
                {
                    _messageBoxService.MessageBoxTypes(login_res.message, StatusCodeSpecs.INVALID_LOGIN.ToString(), MessageTypes.Error);
                }
                else if (login_res.statusCodeSpecs == StatusCodeSpecs.LOGIN_SUCCESS)
                {
                    // Store details 
                    _logonUserModel.token = login_res.keys;
                    _logonUserModel.username = userModel.username;

                    _events.PublishOnUIThread(new HomeScreenPageEvent());
                }
            }
        }

        public void PrintValidationError(ValidationResult data)
        {
            //lbl_signup_validation_errors.Visibility = Visibility.Visible;
            string validation_error = "Error Messages: " + Environment.NewLine;
            // Show Error
            foreach (ValidationFailure failure in data.Errors)
            {
                validation_error += failure.ErrorMessage + Environment.NewLine;
            }
            //lbl_signup_validation_errors.Content = validation_error;
            _messageBoxService.MessageBoxTypes(validation_error, "Validation Error", MessageTypes.Error);
        }
    }
}

﻿using Caliburn.Micro;
using FluentValidation.Results;
using PaperSyncWPF.Logic.Network;
using PaperSyncWPF.Logic.Validation;
using PaperSyncWPF.Misc.EventModels;
using PaperSyncWPF.Misc.Helpers;
using PaperSyncWPF.Models;
using PaperSyncWPF.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.ViewModels.User
{
    public class SignUpViewModel : Screen
    {
        private string _userName;
        private string _passWord;
        private string _confirmPassword;
        private string _emailAddress;

        private IAPIHandler _apiHandler;
        private IEventAggregator _events;
        private IMessageBoxService _messageBoxService;

        public SignUpViewModel(IAPIHandler apiHander, IEventAggregator events, IMessageBoxService messageBoxService)
        {
            _apiHandler = apiHander;
            _events = events;
            _messageBoxService = messageBoxService;
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                NotifyOfPropertyChange(() => UserName);
                _userName = value;
            }
        }

        public string PassWord
        {
            get
            {
                return _passWord;
            }
            set
            {
                NotifyOfPropertyChange(() => PassWord);
                _passWord = value;
            }
        }


        public string ConfirmPassword
        {
            get
            {
                return _confirmPassword;
            }
            set
            {
                NotifyOfPropertyChange(() => ConfirmPassword);
                _confirmPassword = value;
            }
        }


        public string EmailAddress
        {
            get
            {
                return _emailAddress;
            }
            set
            {
                NotifyOfPropertyChange(() => EmailAddress);
                _emailAddress = value;
            }
        }

        public async void SubmitForm()
        {
            // Validate Data Provided asynchronously
            UserModel userModel = new UserModel();
            userModel.username = _userName;
            userModel.password = _passWord;
            userModel.confirmPassword = _confirmPassword;
            userModel.email = _emailAddress;

            var res = await Task.Run(() => new SignUpValidation(_apiHandler).Validate(userModel));
            if (!res.IsValid)
            {
                PrintValidationError(res);
            }
            else
            {
                // Send Data to the API
                var data = _apiHandler.SignUpUser(userModel);
                if (data.statusCodeSpecs == StatusCodeSpecs.UNAVAILABLE_USERNAME)
                {
                    _messageBoxService.MessageBoxTypes(data.message, StatusCodeSpecs.UNAVAILABLE_USERNAME.ToString(), MessageTypes.Error);
                }
                else if(data.statusCodeSpecs == StatusCodeSpecs.USER_NOT_CONFIRM)
                {
                    // Redirect to Verification Page
                    _events.PublishOnUIThread(new SignUpVerificationPageEvent(userModel.username));
                }
            }
        }


        public void PrintValidationError(ValidationResult data)
        {
            //lbl_signup_validation_errors.Visibility = Visibility.Visible;
            string validation_error = "Error Messages: " + Environment.NewLine;
            // Show Error
            foreach (ValidationFailure failure in data.Errors)
            {
                validation_error += failure.ErrorMessage + Environment.NewLine;
            }
            //lbl_signup_validation_errors.Content = validation_error;
            _messageBoxService.MessageBoxTypes(validation_error, "Validation Error", MessageTypes.Error);
        }
    }
}

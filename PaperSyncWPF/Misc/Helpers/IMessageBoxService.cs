﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaperSyncWPF.Misc.Helpers
{
    public interface IMessageBoxService
    {
        void MessageBoxTypes(string text, string caption, MessageTypes type);
        DialogResult GetDialogResults();
    }
}

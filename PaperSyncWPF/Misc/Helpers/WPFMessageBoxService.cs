﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace PaperSyncWPF.Misc.Helpers
{
    public class WPFMessageBoxService : IMessageBoxService
    {
        private DialogResult _dialogResult;
        public void MessageBoxTypes(string text, string title, MessageTypes types)
        {
            switch (types)
            {
                case MessageTypes.Normal:
                    _dialogResult = MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;

                case MessageTypes.Error:
                    _dialogResult = MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        public DialogResult GetDialogResults()
        {
            return _dialogResult;
        }

        public void PrintAPIError()
        {

        }
        
    }
}

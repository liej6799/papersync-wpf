﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.Misc.EventModels
{
    public class SignUpVerificationPageEvent
    {
        public string _username { get; }
        public SignUpVerificationPageEvent(string username)
        {
            _username = username;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.Models
{
    public class APIModels
    {
        public class BaseReturnAPI
        {
            public bool success { get; set; }
            public string message { get; set; }
            public StatusCodeSpecs statusCodeSpecs { get; set; }
        }

        public class LoginModel : BaseReturnAPI
        {
            public string keys { get; set; }
        }

        public class CheckUserExists
        {
            public string Username { get; set; }
        }

        public class ConfirmUser
        {
            public String Username { get; set; }

            public String Code { get; set; }
        }
    }
}

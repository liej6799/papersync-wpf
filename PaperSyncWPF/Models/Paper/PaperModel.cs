﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.Models.Paper
{
    public class PaperModel
    {
        public string file { get; set; }
        public string fileInBase64 { get; set; }
        public string title { get; set; }
        public string author { get; set; }
        public string doi { get; set; }
        public int year { get; set; }
        public string description { get; set; }
    }
}

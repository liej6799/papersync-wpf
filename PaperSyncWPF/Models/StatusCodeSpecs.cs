﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.Models
{
    public enum StatusCodeSpecs
    {
        USER_ENTER_SUCCESS,
        AVAILABLE_USERNAME,
        UNAVAILABLE_USERNAME,
        USER_NOT_CONFIRM,
        INVALID_VERIFICATION,
        LOGIN_REQUIRED,
        LOGIN_SUCCESS,
        INVALID_LOGIN


    }
}

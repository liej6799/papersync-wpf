﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.Models.User
{
    public class LogonUserModel : ILogonUserModel
    {
        public string token { get; set; }
        public string username { get; set; }
    }
}

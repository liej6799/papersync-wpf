﻿namespace PaperSyncWPF.Models.User
{
    public interface ILogonUserModel
    {
        string token { get; set; }
        string username { get; set; }
    }
}
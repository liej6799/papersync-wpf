﻿
using RestSharp;
namespace PaperSyncWPF.Logic.Network
{
    public class NetworkHandler
    {
        private RestClient client;  
        public NetworkHandler()
        {
            Initialize();
        }

        private void Initialize()
        {
            client = new RestClient(Urls.BASE_URL);
        }

        public string CallAPIWithoutHeader(string url, string json)
        {
            var request = new RestRequest(url, Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string CallAPIWithHeader(string url, string json, string token)
        {
            var request = new RestRequest(url, Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            request.AddHeader("Authorization", "bearer " + token);

            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public void CallAPIWithoutHeaderAsync(string url, string json)
        {
            var request = new RestRequest(url, Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);

            client.ExecuteAsync(request, res => {
                var data =  res.Content;
            });
        }
    }
}

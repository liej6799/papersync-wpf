﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.Logic.Network
{
    public class Urls
    {
        //public static readonly string BASE_URL = "http://papersyncapi-dev.ap-southeast-1.elasticbeanstalk.com/api";
        public static readonly string BASE_URL = "https://localhost:44327/api";
        public static readonly string USER = "user/";
        public static readonly string PAPER = "paper/";

        public static string CheckUserExists = USER + "CheckUserExists";
        public static string SignUp = USER + "SignUp";
        public static string Login = USER + "Login";
        public static string ConfirmRegister = USER + "ConfirmRegister";

        public static string AddNewPaper = PAPER + "AddNewPaper";
    }
}

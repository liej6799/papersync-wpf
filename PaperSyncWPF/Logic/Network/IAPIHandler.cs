﻿using PaperSyncWPF.Models;
using PaperSyncWPF.Models.Paper;
using PaperSyncWPF.Models.User;
using System.Threading.Tasks;
using static PaperSyncWPF.Models.APIModels;

namespace PaperSyncWPF.Logic.Network
{
    public interface IAPIHandler
    {
        bool CheckUserExists(string username);
        APIModels.BaseReturnAPI SignUpUser(UserModel userModel);
        APIModels.LoginModel LoginUser(UserModel userModel);
        APIModels.BaseReturnAPI ConfirmRegister(ConfirmUser confirmUser);
        APIModels.BaseReturnAPI AddNewPaper(PaperModel paperModel, string token);
    }
}
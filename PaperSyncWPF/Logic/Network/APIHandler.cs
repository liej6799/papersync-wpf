﻿using Newtonsoft.Json;

using PaperSyncWPF.Models;
using PaperSyncWPF.Models.Paper;
using PaperSyncWPF.Models.User;
using static PaperSyncWPF.Models.APIModels;

namespace PaperSyncWPF.Logic.Network
{
    public class APIHandler : IAPIHandler
    {
        private NetworkHandler _networkHandler;

        public APIHandler()
        {
            _networkHandler = new NetworkHandler();
        }

        public bool CheckUserExists(string username)
        {
            // Transform C# Object to JSON

            var json = JsonConvert.SerializeObject(new APIModels.CheckUserExists()
            {
                Username = username
            });

            var res = _networkHandler.CallAPIWithoutHeader(Urls.CheckUserExists, json);

            return JsonConvert.DeserializeObject<APIModels.BaseReturnAPI>(res).success;
        }

        public APIModels.BaseReturnAPI SignUpUser(UserModel userModel)
        {
            // Transform C# Object to JSON
            var json = JsonConvert.SerializeObject(userModel);
            var res =  _networkHandler.CallAPIWithoutHeader(Urls.SignUp, json);

            return JsonConvert.DeserializeObject<APIModels.BaseReturnAPI>(res);
        }

        public APIModels.LoginModel LoginUser(UserModel userModel)
        {
            // Transform C# Object to JSON
            var json = JsonConvert.SerializeObject(userModel);
            var res = _networkHandler.CallAPIWithoutHeader(Urls.Login, json);

            return JsonConvert.DeserializeObject<APIModels.LoginModel>(res);
        }

        public APIModels.BaseReturnAPI ConfirmRegister(ConfirmUser confirmUser)
        {
            // Transform C# Object to JSON
            var json = JsonConvert.SerializeObject(confirmUser);
            var res = _networkHandler.CallAPIWithoutHeader(Urls.ConfirmRegister, json);

            return JsonConvert.DeserializeObject<APIModels.BaseReturnAPI>(res);
        }

        public APIModels.BaseReturnAPI AddNewPaper(PaperModel paperModel, string token)
        {
            // Transform C# Object to JSON
            var json = JsonConvert.SerializeObject(paperModel);
            var res = _networkHandler.CallAPIWithHeader(Urls.AddNewPaper, json, token);

            return JsonConvert.DeserializeObject<APIModels.BaseReturnAPI>(res);
        }
    }
}

﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PaperSyncWPF.Models.APIModels;

namespace PaperSyncWPF.Logic.Validation
{
    class SignUpVerificationValidation : AbstractValidator<ConfirmUser>
    {
        public SignUpVerificationValidation()
        {
            RuleFor(user => user.Code)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();

            RuleFor(user => user.Username)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
        }
    }
}

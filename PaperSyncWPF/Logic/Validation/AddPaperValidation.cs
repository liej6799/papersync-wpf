﻿using FluentValidation;
using PaperSyncWPF.Models.Paper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.Logic.Validation
{
    class AddPaperValidation : AbstractValidator<PaperModel>
    {
        public AddPaperValidation()
        {
            // All is required
            RuleFor(paper => paper.file)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
            RuleFor(user => user.title)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
            RuleFor(user => user.year)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .GreaterThan(0);
        }
    }
}

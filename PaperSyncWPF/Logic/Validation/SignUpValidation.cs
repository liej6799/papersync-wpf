﻿using FluentValidation;
using PaperSyncWPF.Logic.Network;
using PaperSyncWPF.Models;
using PaperSyncWPF.Models.User;
using PaperSyncWPF.ViewModels.User;
using System.Threading;
using System.Threading.Tasks;

namespace PaperSyncWPF.Logic.Validation
{
    public class SignUpValidation : AbstractValidator<UserModel>
    {
        private IAPIHandler _apiHandler;
        
        public  SignUpValidation(IAPIHandler apiHandler)
        {
            _apiHandler = apiHandler;
            // All is required
            RuleFor(user => user.username)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .Must(CheckUserExists).WithMessage("{PropertyName} existed, please choose another.");
            RuleFor(user => user.password)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .MinimumLength(8);
            RuleFor(user => user.confirmPassword)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .Equal(passwd => passwd.password).WithMessage("{PropertyName} and Password do not match");
                
            RuleFor(user => user.email)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .EmailAddress();
        }

        protected bool CheckUserExists(string username)
        {
            return _apiHandler.CheckUserExists(username);        
        }
    }
}
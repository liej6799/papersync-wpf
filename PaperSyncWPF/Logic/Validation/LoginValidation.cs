﻿using FluentValidation;
using PaperSyncWPF.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperSyncWPF.Logic.Validation
{
    class LoginValidation : AbstractValidator<UserModel>
    {
        public LoginValidation()
        {
            // All is required
            RuleFor(user => user.username)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
            RuleFor(user => user.password)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty();
        }
    }
}
